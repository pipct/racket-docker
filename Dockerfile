FROM debian:sid
MAINTAINER Jamie McClymont <jamie@kwiius.com>

RUN apt-get update && \
    apt-get install -y racket ca-certificates

CMD ["racket"]
